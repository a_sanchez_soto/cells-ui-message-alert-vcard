import { css, } from 'lit-element';


export default css`:host {
  display: block;
  box-sizing: border-box;
  @apply --cells-ui-message-alert-vcard; }

:host([hidden]), [hidden] {
  display: none !important; }

*, *:before, *:after {
  box-sizing: inherit; }

/* The alert message box */
.alert {
  padding: 20px;
  background-color: #f44336;
  /* Red */
  color: white;
  border-bottom: 1px solid #fff;
  opacity: 1;
  transition: opacity 0.6s;
  /* 600ms to fade out */ }

/* The close button */
.closebtn {
  margin-left: 15px;
  color: white;
  font-weight: bold;
  float: right;
  font-size: 22px;
  line-height: 20px;
  cursor: pointer;
  transition: 0.3s; }

/* When moving the mouse over the close button */
.closebtn:hover {
  color: black; }

.alert.success {
  background-color: #4CAF50; }

.alert.info {
  background-color: #2196F3; }

.alert.warning {
  background-color: #ff9800; }

.content-alerts {
  z-index: 10;
  font-family: var(--cells-fontDefault, sans-serif);
  width: 100%;
  position: fixed;
  left: 0px;
 }
`;