import { LitElement, html, } from 'lit-element';
import { getComponentSharedStyles, } from '@cells-components/cells-lit-helpers/cells-lit-helpers.js';
import styles from './cells-ui-message-alert-vcard-styles.js';
/**
This component ...

Example:

```html
<cells-ui-message-alert-vcard></cells-ui-message-alert-vcard>
```

##styling-doc

* @customElement
* @polymer
* @LitElement
* @demo demo/index.html
*/
export class CellsUiMessageAlertVcard extends LitElement {
  static get is() {
    return 'cells-ui-message-alert-vcard';
  }

  // Declare properties
  static get properties() {
    return {
      alerts: {
        type: Array
      },
      position: {
        type: String
      }
    };
  }

  // Initialize properties
  constructor() {
    super();
    this.alerts = [];
  }

  closeAlert(event) {
    let btnClose = event.path[0];
    let parent = btnClose.parentNode;
    this.removeAlert(parent);
  }

  removeAlert(element) {
    element.addEventListener("transitionend", () => {
      element.style.display = "none";
    });
    element.style.opacity = "0";
  }

  add(alert) {
    alert = alert || {};
    let idRamdom = Math.floor(1 + Math.random() * (999999999));
    alert.id = `msg-alert-${idRamdom}`;
    this.alerts.push(alert);
    this.requestUpdate();
    if (alert.delay) {
      setTimeout(() => {
        let parent = this.shadowRoot.querySelector(`#${alert.id}`);
        this.removeAlert(parent);
      }, alert.delay);
    }
  }

  static get shadyStyles() {
    return `
      ${styles.cssText}
      ${getComponentSharedStyles('cells-ui-message-alert-vcard-shared-styles').cssText}
    `;
  }

  // Define a template
  render() {
    return html`
      <style>${this.constructor.shadyStyles}</style>
      <slot></slot>
      <div class = "content-alerts" style = "${this.position === 'top' ? 'top:0px' : 'bottom:0px'}">
      ${this.alerts.map((alert) => html`
        <div id = "${alert.id}" class="alert ${alert.cls ? alert.cls : ''}">
          <span class="closebtn" @click = "${this.closeAlert}" >&times;</span>  
          ${alert.message}
        </div>
      `)}
     </div>
    `;
  }
}

// Register the element with the browser
customElements.define(CellsUiMessageAlertVcard.is, CellsUiMessageAlertVcard);
