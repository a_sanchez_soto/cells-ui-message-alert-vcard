import { setDocumentCustomStyles, } from '@cells-components/cells-lit-helpers/cells-lit-helpers.js';
import { css, } from 'lit-element';

setDocumentCustomStyles(css`
  #iframeBody {
    margin: 0;
  }

  .content-button {
    width: 100%;
    text-align:center;
    margin-top:15px;
    margin-bottom:15px;
  }

  
  .button {
    color: #fff;
    background-color: #f44336; 
    border: none;
    display: inline-block;
    padding: 8px 16px;
    vertical-align: middle;
    overflow: hidden;
    text-decoration: none;
    text-align: center;
    cursor: pointer;
    white-space: nowrap;
  }

  .button.success {
    background-color: #4CAF50;
  }
  
  .button.info {
    background-color: #2196F3;
  }
  
  .button.warning {
    background-color: #ff9800;
  }

  .content-demo-alerts{
    position:relative;
    height:300px;
  }

`);
